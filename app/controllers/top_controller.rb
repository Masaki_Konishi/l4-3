class TopController < ApplicationController
    def main
        if session[:login_uid] == nil
            render  'login'
        else
            render 'main'
        end
    end
    
    def login
        if user = User.find_by(uid: params[:uid])
            if BCrypt::Password.new(user.pass) == params[:pass]
                session[:login_uid] = params[:uid]
                redirect_to '/'
            else
                render 'error'
            end
        else
            render 'error'
        end
    end
    
    def logout
        session.delete(:login_uid)
        redirect_to '/'
    end
    
    def new
        @user = User.new
    end
    
    def create
        user = User.new(uid: params[:uid],pass: BCrypt::Password.create(params[:pass]))
        user.save
        redirect_to top_main_path
    end
end
